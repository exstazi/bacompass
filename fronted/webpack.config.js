const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPluginPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


const path = require('path');

module.exports = {
	entry: {
		index: './src/index.js'
	},

	output: {
		path: __dirname + '/dist/',
		filename: '[name].bundle.js',
		chunkFilename: '[name].bundle.js',
		library: 'app'
	},

	module: {
		rules: [
            {
                test: /\.vue$/,
                exclude: [/node_modules/],
                loader: 'vue-loader'               
            },
			
			{
				test: /\.css$/,
				use: [
				  	MiniCssExtractPlugin.loader,
				  	'css-loader'
				]
			}
		]
	},

	plugins: [
		new VueLoaderPlugin(),
		new MiniCssExtractPlugin({
			filename: 'output.css'
		}),
		new HtmlWebpackPlugin({  
			filename: 'index.html',
			template: 'src/index.html'
		}),
		new CopyPluginPlugin([
			{
				from: './src/assets',
				to: './'
			}, 
			{
				from: './src/index.css',
				to: './'
			}
		])
	],

	resolve: {
		alias: {			
			'@src': path.resolve(__dirname, 'src'),
			'@contents': path.resolve(__dirname, 'src/contents'),
			'@components': path.resolve(__dirname, 'src/components'),
			'@layouts': path.resolve(__dirname, 'src/layouts'),
			'@services': path.resolve(__dirname, 'src/services'),
			'@store': path.resolve(__dirname, 'src/store')
		}
	}
}
