import router from './routes';
import store from './store';

import App from './contents/TheApp';

import MainStyle from './index.css';

const requireComponent = require.context('@components', false, /Base[A-Z]\w+.(vue|js)$/);
requireComponent.keys().forEach(item => {
	const componentName = item.split('/').pop().replace(/\.\w+$/, '');
	const componentConfig = requireComponent(item);

	Vue.component(componentName, componentConfig.default || componentConfig);
});

const requireConfig = require.context('@src/config', false, /\w+.js$/);
requireConfig.keys().forEach(v => requireConfig(v));

//test
import ModalPlugin from '@src/plugins/modal';
Vue.use(ModalPlugin);

export default new Vue({
    el: '#app',

    router,
    store,
    beforeMount() {
        ModalPlugin.rootInstance = this;
    },

    render(h) {
        return h(App);
    }
});