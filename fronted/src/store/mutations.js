import { UPDATE_APP_STATUS } from './mutations-types';

export default
{
    [UPDATE_APP_STATUS](state, status)
    {
        state.appStatus = status;
    }
}