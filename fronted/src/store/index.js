import getters from './getters';
import mutations from './mutations';
import actions from './actions';

import user from './user';

export default new Vuex.Store(
{
    state: 
    {
        appStatus: null
    },

    modules: 
    {
        user
    },

    getters,
    mutations,
    actions
});