import { LOGIN, LOGOUT } from './mutations-types';

export default
{
    [LOGIN](state, user)
    {
        state.id = user.id;
        state.name = user.name;
        state.roles = user.roles || [];
    },

    [LOGOUT](state)
    {
        state.id = null;
        state.name = null;
        state.roles = [];
    }
}