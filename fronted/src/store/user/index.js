import getters from './getters';
import mutations from './mutations';

export default
{
    state:
    {
        id: null,
        name: null,
        roles: [],

        //any user data
    },

    getters,
    mutations
}