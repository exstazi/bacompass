import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Authorization'] = null;

axios.interceptors.response.use(res => res, err => {
	var ex = new Error(err.statusText);
	ex.status = err.response.status;
	ex.data = err.response.data;
	ex.innerError = err;
	ex.isServerError = true;
	
	// if (err.response.status == 401) {
	// 	router.push('/login');
	// 	store.commit('CLEAR_STORE');

    //     ex.message = "Время сессии истекло.";
	// } else if (err.response.status == 403) {
    //     ex.message = "У вас недостаточно прав для выполнения этой операции.";
    // } else if (err.response.status == 404) {
    //     ex.message = "Запрашиваемый обьект не найден или был удален.";
	// } else if (err.response.status >= 500) {
    //     ex.message = "Произошла непредвиденная ошибка. ";
	// }

	return Promise.reject(ex);
});

export default axios;