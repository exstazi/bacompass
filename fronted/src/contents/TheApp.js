import AppService from '@services/AppService';
import SessionService from '@services/SessionService';

import { UPDATE_APP_STATUS } from '@store/mutations-types';

import LoadingContent from '@contents/system/TheLoadingContent.vue';
import AccessDeniedContent from '@contents/system/TheAccessDeniedContent.vue';

export default 
{
    beforeMount() {
        AppService.getStatus()
            .then(status => {
                if (status == 200) {
                    return SessionService.init();
                }
                return status;
            })
            .then(state => {
                if (state === true) {
                    //
                    //this.$router.push('/dashboard');
                } else {
                    //this.$router.push('/login');
                }

                this.$store.commit(UPDATE_APP_STATUS, 200);
            })
            .catch(err => {
                this.$router.push('/login');
                console.error(err);
            });
    },

    computed: {
        appStatus() {
            return this.$store.state.appStatus;
        },

        isAccessAllowed() {
            return true;
        }
    },

    render(h) {
        if (this.appStatus == null) {
            return h(LoadingContent);
        } else {
            if (this.isAccessAllowed) {
                return h('router-view');
            } else {
                return h(AccessDeniedContent);
            }
        }
    }
}