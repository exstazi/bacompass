import axios from 'axios';

let cache = null;

export default
{
    get() {
        if (cache != null) {
            return Promise.resolve(cache);
        }

        return axios.get('/api/glossary')
            .then(data => {
                cache = data.data.content;

                return cache;
            });
    }
}