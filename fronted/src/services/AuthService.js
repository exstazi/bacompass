import axios from 'axios';

export default
{
    getUserByToken(token) {
        //Validate token!!!

        return axios.get('/api/auth/current', {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            })
            .then(res => {
                return {
                    id: res.data.id,
                    name: res.data.name,
                    roles: res.data.roles,
                    token
                }
            })
            .catch(() => null);
    },

    getUserByAccount(username, userpassword) {
        return axios.post('/api/auth/login', {username, userpassword})
            .then(res => {

            })
            .catch(() => null);
    }
}