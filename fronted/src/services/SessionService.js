import AuthService from './AuthService';

import store from '@store/';
import { LOGIN } from '@store/mutations-types';

export default
{
    /**
     * Инициализация приложения. 
     * Выполняется при первой загрузке.
     * @return Promise
     */
    init() {
        let token = localStorage.getItem('token');

        if (token == null) {
            return Promise.resolve(false);
        }

        return AuthService.getUserByToken(token)
            .then(user => {
                if (user && user.id) {
                    return this.initUser(user);
                }
                return false;
            })
    },

    /**
     * Инциалзация приложения для пользователя
     * @param {*} user 
     */
    initUser(user) {
        this.login(user);

        return Promise.resolve(true);
    },

    login(user) {
        localStorage.setItem('token', user.token);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + user.token;

        store.commit(LOGIN, user);
    },

    logout() {
        localStorage.removeItem('token');
        axios.defaults.headers.common['Authorization'] = null;

        store.commit('LOGOUT');
    }
}