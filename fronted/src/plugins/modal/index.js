import ModalContainer from './ModalContainer.vue';

const Plugin =
{
    install(Vue, opt)
    {
        const createContainer = function () {
            const div = document.createElement('div');
            document.body.appendChild(div);

            return div;
        }

        this.rootInstance = null;

        Vue.prototype.$modal = 
        {
            show(component, title, props, width = '680px', height = 'auto') {
                const container = createContainer();
                
                return new Promise((resolve, reject) => {
                    let vm = new Vue({
                        parent: Plugin.rootInstance,
                        render: h => h(ModalContainer, {
                            props: {title, width, height},
                            on: {
                                $cancel() {
                                    vm.$el.remove();
                                    vm.$destroy();
                                    reject();
                                }
                            }
                        }, [h(component, {
                            props,
                            on: {
                                $ok(payload) {
                                    vm.$el.remove();
                                    vm.$destroy(); 
                                    resolve(payload);
                                },
                                $cancel(payload) {
                                    vm.$el.remove();
                                    vm.$destroy();
                                    reject(payload);
                                }
                            }
                        })])
                    }).$mount(container);
                });                
            }
        }

        
        Vue.component('ModalContainer', ModalContainer);
    }
}

export default Plugin;