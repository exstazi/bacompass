import AppLayout from '@layouts/TheAppLayout.vue';

import DashboardContent from '@contents/TheDashboardContent.vue';
import CompetencyModelContent from '@contents/TheCompetencyModelContent.vue';

import LoginContent from '@contents/TheLoginContent.vue';
import NotFoundContent from '@contents/system/TheNotFoundContent.vue';

export default
[
    {
        path: '/',
        component: AppLayout,
        children: [
            {
                path: '',
                redirect: '/dashboard'
            },

            {
                path: 'dashboard',
                component: DashboardContent,
                meta: {
                    title: 'Dashboard',
                    auth: false
                }
            },
            {
                path: 'competency-model',
                component: CompetencyModelContent,
                meta: {
                    title: 'Competency model'
                }
            }
        ]
    },

    {
        path: '/login',
        component: LoginContent
    },

    {
        path: '*',
        component: NotFoundContent
    }
]