import routes from './routes';
import store from '@store/';

import config from '@src/config/config.json';

const router = new VueRouter({
  mode: 'history',
  routes,
  linkActiveClass: 'active'
});

router.beforeEach((to, _, next) => {
	if (to.meta.title) {
		document.title = `${to.meta.title} | ${config.title}`;
	} else {
		document.title = config.title;
    }

    if (to.matched.some(v => v.meta.auth) && store.getters.getCurrentUser == null) {
        next('/login');
	} else {
		next();
    }
});

export default router;