mv $REMOTE_APP_PATH/app_build.tar.gz $REMOTE_APP_PATH/$APP_NAME.tar.gz
docker-compose -f /home/docker-compose.yml down
docker stop $APP_NAME 
docker rm $APP_NAME
docker rmi $APP_NAME
docker load --input $REMOTE_APP_PATH/$APP_NAME.tar.gz
docker-compose -f /home/docker-compose.yml up -d