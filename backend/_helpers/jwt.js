const expressJwt = require('express-jwt');
const config = require('@config');

module.exports = function() 
{
    return expressJwt({ secret: config.secure.secret })
        .unless({
            path: [
                // public routes that don't require authentication
                '/api/auth/login',
                '/api/auth/register',
                '/api/auth/password_recovery',
                '/api/auth/repeat_invite',
                '/api/auth/confirm_email'
            ]
        });
}