/*
 * BotFather
 * Copyright(c) 2016 Aleki
 * MIT Licensed
 */
"use strict"

const path = require("path")
const https = require("https")
const crypto = require("crypto")

class BotFather {

	/**
	 * @param {string} token
	 * @see https://core.telegram.org/bots#6-botfather
	 */
	constructor(token) {
		this.token = token
		this.path = "/bot" + token + "/"
		this.options = {
			host: "api.telegram.org",
			port: 443,
			method: "POST"
		}
	}

	startPolling() {
        this.getUpdates()
    }

	editMessageText(chat_id, message_id, options) {
		return this.api('editMessageText', Object.assign({chat_id, message_id}, options))
					.then(json => {
						if (json.ok) {
							return json.result
						}
						throw new Error(json.description)
					})
	}

	/**
	 * @param {string} method
	 * @param {Object} parameters
	 * @return {Promise}
	 * @see https://core.telegram.org/bots/api#making-requests
	 */
 	api(method, parameters = {}) {
		return new Promise((resolve, reject) => {
			const timeout = (parameters.timeout || 60 * 2) * 1000
			const boundary = crypto.randomBytes(32).toString("hex")

			this.options.path = this.path + method;
			this.options.headers = {
				"Content-Type": `multipart/form-data; boundary=${boundary}`
			}

			if(!Object.keys(parameters).length) {
				this.options.method = "GET"
				this.options.headers = {}
			}

			let request = https.request(this.options, (response) => {
				let data = ""
				response.on("data", (chunk) => {
					data += chunk
				})
				response.on("end", () => {
				try {
					data = JSON.parse(data)
				} catch (exception) {
					reject(exception)
				}
					resolve(data)
				})
			})

			request.on("error", (exception) => {
				reject(exception)
			})

			request.setTimeout(timeout)

			if(!Object.keys(parameters).length) {
				return request.end()
			}

			let buffers = []
			const names = Object.keys(parameters)
			const promises = names.map((name) => new Promise((resolve, reject) => {
				let parameter = parameters[name]
				let buffer = Buffer.from(`--${boundary}\r\n`)
				switch(typeof parameter) {
					case "string":
					case "number":
					case "boolean":
						buffer = Buffer.concat([buffer, Buffer.from(
							`Content-Disposition: form-data; name="${name}"\r\n\r\n` +
							`${parameter}\r\n`
						)])
						buffers.push(buffer)
						resolve()
					break
					case "array":
					case "object":
						if(parameter.constructor.name != "ReadStream") {
							parameter = JSON.stringify(parameter)
							buffer = Buffer.concat([buffer, Buffer.from(
								`Content-Disposition: form-data; name="${name}"\r\n\r\n` +
								`${parameter}\r\n`
							)])
							buffers.push(buffer)
							resolve()
						} else {
							const file = parameter
							const filename = path.basename(file.path)
							buffer = Buffer.concat([buffer, Buffer.from(
								`Content-Disposition: form-data; name="${name}"; filename="${filename}"\r\n` +
								`Content-Type: application/octet-stream\r\n\r\n`
							)])
							file
								.on("data", (chunk) => {
									buffer = Buffer.concat([buffer, Buffer.from(chunk)])
								})
								.on("end", () => {
									buffer = Buffer.concat([buffer, Buffer.from("\r\n")])
									buffers.push(buffer)
									resolve()
								})
						}
					break
				}
			}))
			Promise.all(promises)
				.then(() => {
					buffers.push(Buffer.from(`--${boundary}--\r\n`))
					const buffer = Buffer.concat(buffers)
					request.end(buffer)
				})
				.catch((exception) => {
					reject(exception)
				})
		})
	}

	send(method, parameters = {}) {
		return this.api(method, parameters)
			.then(json => {
				if (json.ok) {
					return json.result
				}
				throw new Error(json.description)
			})
	}

	/**
	 * @param {Object} parameters
	 * @see https://core.telegram.org/bots/api#getupdates
	 */
	getUpdates(parameters = {limit: 100, timeout: 60 * 2}) {
		this.send('getUpdates', parameters)
			.then(updates => {
				for (let update of updates) {
					this.onReceiveUpdate(update)
				}
				// offset = update_id of last processed update + 1
				if (updates.length > 0) {
					const identifiers = updates.map((update) => update.update_id)
					parameters.offset = Math.max.apply(Math, identifiers) + 1
				}
				this.getUpdates(parameters)
			})
			.catch(exception => {
				console.error(exception.stack)
				setTimeout(() => this.getUpdates(parameters), 5000)
			})
    }
    
    onReceiveUpdate(update) {
        //console.log(update);
    }
}

module.exports = BotFather