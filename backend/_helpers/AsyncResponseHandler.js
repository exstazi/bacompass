/*
GET: [] => 200
GET: null => 404

POST: {} => 201
POST: exception

PUT: {} => 200
PUT: null => 404

DELETE: {} => 201
DELETE: null => 404

*/

const MapCodeFromObject =
{
    'GET': 200,
    'POST': 201,
    'PUT': 202,
    'DELETE': 204
};

const MapCodeFromErrorObject =
{
    'NotFoundException': 404,
    'ConflictException': 409
}

module.exports = function (executer)
{
    return async function (req, res)
    {
        let payload = {
            execTime: Date.now(),
            code: 200
        };

        try
        {
            payload.content = await executer(req);

            if (payload.content == null) {
                payload.code = 404;
            } else {
                if (req.method in MapCodeFromObject) {
                    payload.code = MapCodeFromObject[req.method];
                }
            } 
        } catch (err)
        {
            console.error(err);

            if (typeof(err) === 'string') {
                payload.code = 404;
            } else if (err.name in MapCodeFromErrorObject) {
                payload.code = MapCodeFromErrorObject[err.name];
            } else {
                payload.code = 500;
            }

            payload.err = err;
        }

        payload.execTime = Date.now() - payload.execTime;

        res.status(payload.code)
            .json(payload);
    }
}