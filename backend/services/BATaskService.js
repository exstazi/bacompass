const BATaskRepository = require('@repositories/BATaskRepository');

module.exports.get = async function()
{
    return await BATaskRepository.findAll();
}