const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const config = require('@config');

let createHash = function (login, pwd)
{
    let hash = crypto.createHmac('sha512', login);
    hash.update(pwd);

    let value = hash.digest('hex');
    return value;
}

module.exports.login = async function ({login, pwd})
{
    //1. Validate login and pwd

    let pwdHash = createHash(login, pwd);



    let token = jwt.sign(
        {
            id: user.id,
            email: user.email,
            role: user.role,
            name: encodeURIComponent(user.name)
        },
        config.secure.secret,
        {
            expiresIn: config.secure.expiresIn
        });
    
    return token;
}