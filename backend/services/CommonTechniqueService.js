const CommonTechniqueRepository = require('@repositories/CommonTechniqueRepository');

module.exports.get = async function ()
{
    return await CommonTechniqueRepository.findAll();
}