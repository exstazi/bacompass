const KnowledgeAreaRepository = require("@repositories/KnowledgeAreaRepository");
const BATaskRepository = require("@repositories/BATaskRepository");
const ProficiencyLevelRepository = require("@repositories/ProficiencyLevelRepository");
const ProficiencyDescriptorRepository = require("@repositories/ProficiencyDescriptorRepository");
const CommonTechniqueRepository = require("@repositories/CommonTechniqueRepository");
const CommonTechniqueUsageRepository = require("@repositories/CommonTechniqueUsageRepository");
const TechniqueProficiencyLevelRepository = require("@repositories/TechniqueProficiencyLevelRepository");

let glossary = null;

module.exports.get = async function()
{
    if (glossary != null) {
        return Promise.resolve(glossary);
    }

    let knowledgeAreas = await KnowledgeAreaRepository.findAll({order: ['Id'], raw: true});
    let baTasks = await BATaskRepository.findAll({order: ['Id'], raw: true});
    let proficiencyLevels = await ProficiencyLevelRepository.findAll({order: ['Id'], raw: true});
    let proficiencyDescriptors = await ProficiencyDescriptorRepository.findAll({order: ['ProficiencyLevelId'], raw: true});

    proficiencyDescriptors.forEach(v => {
        v.Descriptor = v.Descriptor.replace(/�/g, "'");
    });

    let commonTechniques = await CommonTechniqueRepository.findAll({order: ['Id'], raw: true});
    let commonTechniqueUsages = await CommonTechniqueUsageRepository.findAll({order: ['CommonTechniquesId'], raw: true});
    let techniqueProficiencyLevels = await TechniqueProficiencyLevelRepository.findAll({order: ['Id'], raw: true});

    glossary = {
        knowledgeAreas,
        baTasks,
        proficiencyLevels,
        proficiencyDescriptors,
        commonTechniques,
        commonTechniqueUsages,
        techniqueProficiencyLevels
    }

    return glossary;
}

module.exports.resetCache = async = function()
{
    glossary = null;
    return {};
}