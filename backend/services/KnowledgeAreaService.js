const KnowledgeAreaRepository = require('@repositories/KnowledgeAreaRepository');

module.exports.get = async function()
{
    return await KnowledgeAreaRepository.findAll();
}