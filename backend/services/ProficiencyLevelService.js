const ProficiencyLevelRepository = require('@repositories/ProficiencyLevelRepository');

module.exports.get = async function()
{
    return await ProficiencyLevelRepository.findAll();
}