const express = require('express');
const router = express.Router();

const CommonTechniqueService = require('@services/CommonTechniqueService');
const AsyncReponseHandler = require('../_helpers/AsyncResponseHandler');

router.route('/')
    .get(AsyncReponseHandler(() => CommonTechniqueService.get()));

module.exports = router;
