const express = require('express');
const router = express.Router();

const KnowledgeAreaService = require('@services/KnowledgeAreaService');
const AsyncReponseHandler = require('../_helpers/AsyncResponseHandler');

router.route('/')
    .get(AsyncReponseHandler(() => KnowledgeAreaService.get()));

module.exports = router;
