const express = require('express');
const router = express.Router();

const ProficiencyLevelService = require('@services/ProficiencyLevelService');
const AsyncReponseHandler = require('../_helpers/AsyncResponseHandler');

router.route('/')
    .get(AsyncReponseHandler(() => ProficiencyLevelService.get()));

module.exports = router;
