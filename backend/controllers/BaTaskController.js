const express = require('express');
const router = express.Router();

const BATaskService = require('@services/BATaskService');
const AsyncReponseHandler = require('../_helpers/AsyncResponseHandler');

router.route('/')
    .get(AsyncReponseHandler(() => BATaskService.get()));

module.exports = router;
