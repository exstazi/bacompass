const express = require('express');
const router = express.Router();

const GlossaryService = require('@services/GlossaryService');
const AsyncReponseHandler = require('../_helpers/AsyncResponseHandler');

router.route('/')
    .get(AsyncReponseHandler(() => GlossaryService.get()));

router.route('/reset-cache')
    .get(AsyncReponseHandler(() => GlossaryService.resetCache()));

module.exports = router;
