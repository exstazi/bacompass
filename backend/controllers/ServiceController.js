const express = require('express');
const router = express.Router();


router.route('/state')
    .get((req, res) => res.status(200).json({}));

module.exports = router;
