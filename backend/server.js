const PATH_TO_CONTROLLER_DIR = '/controllers';

const express = require('express');
const http = require('http');
const fs = require('fs');

const app = express();

app.use(express.json());

//for test only
app.use(express.static('../fronted/dist'));

// app.use(require('./_helpers/jwt')());

//Load all controllers
fs.readdirSync(__dirname + PATH_TO_CONTROLLER_DIR)
    .forEach(v => {
        let match = v.match(/([A-Z]\w+)(Controller.js)$/);

        if (match != null) {
            let snakeCase = match[1]
                .replace(/[A-Z]/g, v => '-' + v.toLowerCase())
                .substring(1);

            let module = require(__dirname + PATH_TO_CONTROLLER_DIR + '/' + v);

            if (typeof module === 'function' && module.name === 'router') {
                console.log('Register routes: /api/' + snakeCase);
                app.use('/api/' + snakeCase, module);
            } else {
                console.error(`Incorrect controller module: ${v}`);
            }
        }
    });


app.use((req, res, next) => {
    res.status(405).json({code: 405, responce: null});
});

app.use(require('./_helpers/errorHandlers'));

module.exports.init = function (port) {
    return new Promise((resolve, reject) => {
        http.createServer(app).listen(port, () => {
            console.log(`Server listen ${port} port.`);

            resolve();
        });
    });  
}