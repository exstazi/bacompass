const DataTypes = require('sequelize').DataTypes;
const sequelize = require('./index');

const ModelName = __filename.replace(/^.*[\\\/]/, '').replace(/(Repository.js|.js)$/, '');
const Model = sequelize.define(ModelName, {
    Id: 
    {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    Name:
    {
        type: DataTypes.STRING,
        allowNull: true
    },
    Definition:
    {
        type: DataTypes.STRING,
        allowNull: true
    }
});

module.exports = Model;
