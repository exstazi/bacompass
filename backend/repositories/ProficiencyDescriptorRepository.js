const DataTypes = require('sequelize').DataTypes;
const sequelize = require('./index');

const BATaskModel = require('./BATaskRepository');
const ProficiencyLevelModel = require('./ProficiencyLevelRepository');

const ModelName = __filename.replace(/^.*[\\\/]/, '').replace(/(Repository.js|.js)$/, '');
const Model = sequelize.define(ModelName, {
    BATaskId: 
    {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    ProficiencyLevelId:
    {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isInt: true
        }
    },
    Descriptor:
    {
        type: DataTypes.STRING,
        allowNull: true
    }
});

Model.belongsTo(BATaskModel, {foreignKey: 'BATaskId', targetKey: 'Id'});
Model.belongsTo(ProficiencyLevelModel, {foreignKey: 'ProficiencyLevelId', targetKey: 'Id'});

module.exports = Model;
