const DataTypes = require('sequelize').DataTypes;
const sequelize = require('./index');

const ProficiencyLevelModel = require('./ProficiencyLevelRepository');

const ModelName = __filename.replace(/^.*[\\\/]/, '').replace(/(Repository.js|.js)$/, '');
const Model = sequelize.define(ModelName, {
    Id: 
    {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    ProficiencyLevelId:
    {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isInt: true
        }
    },
    Definition:
    {
        type: DataTypes.STRING,
        allowNull: true
    }
});

Model.belongsTo(ProficiencyLevelModel, {foreignKey: 'ProficiencyLevelId', targetKey: 'Id'});

module.exports = Model;
