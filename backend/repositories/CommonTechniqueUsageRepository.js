const DataTypes = require('sequelize').DataTypes;
const sequelize = require('./index');

const BATaskModel = require('./BATaskRepository');
const CommonTechniqueModel = require('./CommonTechniqueRepository');

const ModelName = __filename.replace(/^.*[\\\/]/, '').replace(/(Repository.js|.js)$/, '');
const Model = sequelize.define(ModelName, {
    BATaskId: 
    {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    CommonTechniquesId:
    {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isInt: true
        }
    },
    Usage:
    {
        type: DataTypes.STRING,
        allowNull: true
    }
});

Model.belongsTo(BATaskModel, {foreignKey: 'BATaskId', targetKey: 'Id'});
Model.belongsTo(CommonTechniqueModel, {foreignKey: 'CommonTechniquesId', targetKey: 'Id'});

module.exports = Model;
