const DataTypes = require('sequelize').DataTypes;
const sequelize = require('./index');

const KnowledgeAreaRepository = require('./KnowledgeAreaRepository');

const ModelName = __filename.replace(/^.*[\\\/]/, '').replace(/(Repository.js|.js)$/, '');
const Model = sequelize.define(ModelName, {
    Id: 
    {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    KnowledgeAreaId:
    {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isInt: true
        }
    },
    Name:
    {
        type: DataTypes.STRING,
        allowNull: true
    },
    Definition:
    {
        type: DataTypes.STRING,
        allowNull: true
    }
});

Model.belongsTo(KnowledgeAreaRepository, {foreignKey: 'KnowledgeAreaId', id: 'Id'});

module.exports = Model;
