require('module-alias/register');

const server = require('@/server');
const config = require('@/config');

server.init(config.server.port);
