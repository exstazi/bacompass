const environment = process.env.NODE_ENV || 'development';

if (environment == 'production') {
    module.exports = require('./production/config.json');
} else {
    module.exports = require('./developer/config.json');
}